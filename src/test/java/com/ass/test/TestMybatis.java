package com.ass.test;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ass.common.generated.dao.TTestMapper;
import com.ass.common.generated.model.TTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" ,
		"classpath:config/aC-common.xml",
		//"classpath:config/aC-quartz-config.xml",
		"classpath:config/aC-shiro.xml",
		//"classpath:config/ehcache.xml",
		"classpath:config/mybatisSqlMapConfig.xml"
		
})
public class TestMybatis {

	private static final Logger logger = Logger.getLogger(TestMybatis.class);

	private TTestMapper tTestMapper;
	
	public TTestMapper gettTestMapper() {
		return tTestMapper;
	}

	@Autowired
	public void settTestMapper(TTestMapper tTestMapper) {
		this.tTestMapper = tTestMapper;
	}


	@Test
	public void test1() {
//		ApplicationContext aCtx = new FileSystemXmlApplicationContext("classpath:applicationContext.xml", "classpath: config/*.xml");
//		TTestMapper tTestMapper = (TTestMapper) aCtx.getBean("tTestMapper");
		
		
		TTest t = new TTest();
		t.setName1("name1");
		t.setName2("name2test2");
		tTestMapper.insertSelective(t);
		logger.debug("success~~~~~~~~~~~");
	}

}
