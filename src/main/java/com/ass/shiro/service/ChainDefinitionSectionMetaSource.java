package com.ass.shiro.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.config.Ini;
import org.apache.shiro.config.Ini.Section;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.web.config.IniFilterChainResolverFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ass.common.generated.dao.TPermissionMapper;
import com.ass.common.generated.model.TPermission;
import com.ass.common.generated.model.TPermissionExample;
import com.ass.common.utils.StringUtil;


/**
 * 借助spring {@link FactoryBean} 对apache shiro的premission进行动态创建 动态的从数据库中读取权限信息
 * 
 * @author wangt
 * 
 */
@Component
public class ChainDefinitionSectionMetaSource implements
		FactoryBean<Ini.Section> {

	public static int i;
	

	// shiro默认的链接定义 写在xml上的。
	private String filterChainDefinitions;
	
	@Resource
	private TPermissionMapper tPermissionMapper;

	/**
	 * 通过filterChainDefinitions对默认的链接过滤定义
	 * 
	 * @param filterChainDefinitions
	 *            默认的接过滤定义
	 */
	public void setFilterChainDefinitions(String filterChainDefinitions) {
		this.filterChainDefinitions = filterChainDefinitions;
	}

	@Override
	public Section getObject() throws BeansException {
		Ini ini = new Ini();
		// 加载默认的url
		ini.load(filterChainDefinitions);
		System.out.println(filterChainDefinitions);
		/*1加载类似以下的信息
		  	/login.do = authc 
	        /favicon.ico = anon
	        /logout.do = logout
	        /selectOption.do = roles[index]
	        /index.jsp = perms[index:index]
			/testDwr.jsp = perms[index:testdwr]
	          
	         2
	         	循环数据库资源的url
	        for (Resource resource : resourceDao.getAll()) {
	if(StringUtils.isNotEmpty(resource.getValue()) && StringUtils.isNotEmpty(resource.getPermission())) {
	        		section.put(resource.getValue(), resource.getPermission());
	        	}
	        }
			加载数据库t_permission 的 value 和 permission组成类似1的格式 ，
			若要这样使用， permission 需要--->  perms[permission]

		 */
		Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);
		
		//查询数据库中  路径对应需要的权限.
		TPermissionExample example = new TPermissionExample();
		example.createCriteria().andPermissionIsNotNull().andValueIsNotNull().andNameIsNotNull();
		List<TPermission> lst = tPermissionMapper.selectByExample(example);
		for(TPermission per : lst){
			//访问某一路径，需要对应的权限
			if(StringUtil.isNotEmpty(per.getValue())&&StringUtil.isNotEmpty(per.getPermission()))
				section.put(per.getValue(), "perms["+per.getPermission()+"]");
			
		}
		//section.put("/testDwr.jsp", "perms[index:testdwr]");///testDwr.jsp = perms[index:testdwr]
		
		/*//因为顺序原因, 把/**放到最后
		 *   [上面的覆盖下面的]
		 *  把("/**", "authc") 放在  ("/testDwr.jsp", "perms[index:testdwr]")  上面,
		 *  /testDwr.jsp 就只需要登录, 不需要perms[index:testdwr]权限了
		 */
		section.put("/**", "anon");
		for(String s : section.keySet()){
			System.out.println(s + "----"+ section.get(s)+"--------22222---section");
		}
		System.out.println("11111111111111111111111");
		
		return section;
	}

	@Override
	public Class<?> getObjectType() {
		return Section.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
